import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection} from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

  
  books = [{title:'Alice in Wonderland', author:'Lewis Carrol', summary:"Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."},
  {title:'War and Peace', author:'Leo Tolstoy',summary:"It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged"},
  {title:'The Magic Mountain', author:'Thomas Mann',summary:" It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."}];


  public addbooks(){
    setInterval(()=>this.books.push({title:'A new one',author:'New Author', summary:"Short Summary"}),2000);
  }
  /*

  public getBooks(){
    const booksObservable = new Observable(obs => {
      setInterval(()=> obs.next(this.books),500)
    });
    return booksObservable;

  }

*/
bookCollection!:AngularFirestoreCollection;
  public getBooks(userID: any){
    this.bookCollection = this.db.collection(`users/${userID}/books`);
    return this.bookCollection.snapshotChanges().pipe(map(
      collection => collection.map(
        document => {
          const data = document.payload.doc.data();
          data.id = document.payload.doc.id;
          return data;
        }
      )
    )) 
  }


  

  constructor(private db:AngularFirestore) { }
}
