import { AuthService } from './../auth.service';
import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';


@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  //books: any;
  books$: any; 
  userID: any;

  panelOpenState = false;
  constructor(private booksService:BooksService, public authService:AuthService) { }

  ngOnInit(): void {
    //this.booksService.addbooks();
    //this.books$=this.booksService.getBooks();
    //this.books$.subscribe((books: any) => this.books = books);
    this.authService.getUser().subscribe(
      user => {
        this.userID = user?.uid;
        this.books$ = this.booksService.getBooks(this.userID)
      }
    )
    
  }

}
