import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'classify',
  templateUrl: './classify.component.html',
  styleUrls: ['./classify.component.css']
})


export class ClassifyComponent implements OnInit {
  favoriteSeason: string | undefined;
  selectedNetwork: any; 
  networks: string[] = ['BBC', 'CNN', 'ABC'];

  constructor(private route:ActivatedRoute) { }

  ngOnInit(): void {
    this.selectedNetwork=this.route.snapshot.params.network;
  }

}
